//
//  CastlabsSDKOMA.h
//  CastlabsSDK
//
//  Created by Guido Parente on 22/01/2016.
//  Copyright © 2016 castLabs. All rights reserved.
//

// Umbrella header

#ifndef CastlabsSDKOMA_h
#define CastlabsSDKOMA_h

#import "CastlabsSDK/CastlabsSDK.h"

/*!
 @interface  CLOmaInit
 
 @brief Umbrealla header for OMA DRM plugin
 
 
 @copyright  Copyright © 2015 castLabs
 */
@interface CastlabsOMA : NSObject < CLSDKPlugin>

-(void) initPlugin;

@end
#endif /* Header_h */
