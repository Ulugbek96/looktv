//
//  AnimatedSingleViewController.swift
//  Final_Look
//
//  Created by Mirzoulugbek Yusupov on 7/15/19.
//  Copyright © 2019 Mirzoulugbek Yusupov. All rights reserved.
//
import UIKit
import SDWebImage
import NVActivityIndicatorView
import MediaPlayer

var count: Int = 0

class AnimatedSingleViewController: UINavigationController, UIGestureRecognizerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, NVActivityIndicatorViewable{
    
    var productId: String?
    var type: String?
    let videoController = PlayerViewController()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    var snapShotHandler: ((SingleSnapshotCell, IndexPath)->())?
    
    var singleSelectImageHandler: ((SingleSnapshotCell, IndexPath) -> ())?
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! SingleSnapshotCell
        
        snapShotHandler?(cell, indexPath)
        
        if indexPath.row == 0 {
            cell.addSubview(selectedTransparentView)
            selectedTransparentView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor)
            selectedTransparentView.backgroundColor = .clear
            selectedTransparentView.layer.borderColor = #colorLiteral(red: 0, green: 0.8994353414, blue: 0.7261761427, alpha: 1).cgColor
            selectedTransparentView.layer.borderWidth = 2
            selectedTransparentView.layer.cornerRadius = 3
        
            cell.addSubview(videoPlayButton)
            videoPlayButton.centerXInSuperview()
            videoPlayButton.centerYInSuperview()
            videoPlayButton.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, padding: .init(), size: CGSize(width: 40, height: 40))
            videoPlayButton.layer.cornerRadius = 40 / 2
            videoPlayButton.addTarget(self, action: #selector(handleVideo), for: .touchUpInside)
        }
        return cell
    }
    
    
    
    @objc func handleVideo() {
        self.present(videoController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SingleSnapshotCell
        
        cell.addSubview(selectedTransparentView)
        selectedTransparentView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor)
        selectedTransparentView.backgroundColor = .clear
        selectedTransparentView.layer.borderColor = #colorLiteral(red: 0, green: 0.8994353414, blue: 0.7261761427, alpha: 1).cgColor
        selectedTransparentView.layer.borderWidth = 2
        selectedTransparentView.layer.cornerRadius = 3
        
        singleSelectImageHandler?(cell, indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    //MARK:-Labels
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = #colorLiteral(red: 0.345725596, green: 0.3542075157, blue: 0.3954209089, alpha: 1)
        scroll.showsVerticalScrollIndicator = false
        scroll.isScrollEnabled = true
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    
    let genreLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let directorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let runTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let releasedLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let actorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 10
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let storyLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 10
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    //MARK:- round image view
    let genreRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let directorRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let runtimeRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let releasedRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let actorRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let storyRound: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "header_round").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    //MARK:- Hard code labels, immutable
    let genre: UILabel = {
        let label = UILabel()
        label.text = "Genre"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    let director: UILabel = {
        let label = UILabel()
        label.text = "Director"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    let runTime: UILabel = {
        let label = UILabel()
        label.text = "Runtime"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    let released: UILabel = {
        let label = UILabel()
        label.text = "Released"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    let actor: UILabel = {
        let label = UILabel()
        label.text = "Actor"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    let story: UILabel = {
        let label = UILabel()
        label.text = "Story"
        label.textColor = .lightGray
        label.font = UIFont(name: "Verdana", size: 9)
        return label
    }()
    
    //MARK:- play and download button
    let playButton: UIButton = {
        let button = UIButton(type: .system)
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 0 / 255, green: 209 / 255, blue:  102 / 255, alpha: 1)
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        button.contentMode = .scaleAspectFill
        return button
    }()
    
    let playImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "btn_play_icon")
        image.clipsToBounds = true
        return image
    }()
    let downloadImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "btn_download_icon")
        image.clipsToBounds = true
        return image
    }()
    
    let downloadButton: UIButton = {
        let button = UIButton(type: .system)
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 0 / 255, green: 140 / 255, blue: 218 / 255, alpha: 1)
        button.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(handleDownload), for: .touchUpInside)
        return button
    }()
    
    let downloadLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.text = "DOWNLOAD"
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .white
        label.contentMode = .scaleAspectFill
        return label
    }()
    
    let playLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.text = "PLAY"
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .white
        label.contentMode = .scaleAspectFill
        return label
    }()
    
    let videoPlayButton: UIButton = {
        let button = UIButton(type: .custom)
        button.clipsToBounds = true
        button.backgroundColor = .clear
        button.setImage(#imageLiteral(resourceName: "player_icon"), for: .normal)
        button.contentMode = .scaleAspectFill
        return button
    }()
    
    let dismissButton: UIButton = {
        let button = UIButton()
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.setImage(#imageLiteral(resourceName: "icon_category_open"), for: .normal)
        return button
    }()
    
    //MARK:- portrait and landscape imageVIew
    let portraitImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.layer.cornerRadius = 8
        return iv
    }()
    
    let landscapeImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    
    let imageLabel: UILabel = {
        let label = UILabel()
        label.text = "Images"
        label.font = UIFont(name: "Verdana", size: 16)
        label.textColor = .white
        label.clipsToBounds = true
        label.contentMode = .left
        return label
    }()
    
    let topView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.2507136166, green: 0.2596068382, blue: 0.3075306118, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.345725596, green: 0.3542075157, blue: 0.3954209089, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let snapshotImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 5
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let selectedTransparentView: UIView = {
        let view = UIView()
        return view
    }()
    
    
    
    //MARK:- setup methods
    fileprivate func setupViews() {
        topView.addSubview(downloadButton)
        downloadButton.anchor(top: nil, leading: portraitImageView.trailingAnchor, bottom: portraitImageView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 20, bottom: 0, right: 0), size: CGSize(width: 85, height: 25))
        downloadButton.layer.cornerRadius = 2
        
        topView.addSubview(playButton)
        playButton.anchor(top: nil, leading: nil, bottom: downloadButton.bottomAnchor, trailing: topView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20), size: CGSize(width: 85, height: 25))
        playButton.layer.cornerRadius = 2
        
        downloadButton.addSubview(downloadImage)
        downloadImage.anchor(top: downloadButton.topAnchor, leading: downloadButton.leadingAnchor, bottom: downloadButton.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 5, right: 0))
        
        playButton.addSubview(playImage)
        playImage.anchor(top: playButton.topAnchor, leading: playButton.leadingAnchor, bottom: playButton.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 5, right: 0))
        
        playButton.addSubview(playLabel)
        playLabel.anchor(top: playButton.topAnchor, leading: playImage.trailingAnchor, bottom: playButton.bottomAnchor, trailing: playButton.trailingAnchor, padding: .init(top: 5, left: 8, bottom: 5, right: 0))
        
        downloadButton.addSubview(downloadLabel)
        downloadLabel.anchor(top: downloadButton.topAnchor, leading: downloadImage.trailingAnchor, bottom: downloadButton.bottomAnchor, trailing: downloadButton.trailingAnchor, padding: .init(top: 6, left: 3, bottom: 5, right: 0))
    }
    
    fileprivate func setupLabelsAndButtons() {
        
        let roundStackView = UIStackView(arrangedSubviews: [genreRound,directorRound,runtimeRound,releasedRound])
        topView.addSubview(roundStackView)
        roundStackView.anchor(top: landscapeImageView.bottomAnchor, leading: portraitImageView.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 12, left: 8, bottom: 0, right: 0), size: CGSize(width: 5, height: 0))
        roundStackView.distribution = .equalSpacing
        roundStackView.spacing = 13
        roundStackView.axis = .vertical
        
        
        let labelStackView = UIStackView(arrangedSubviews: [genre, director, runTime, released])
        topView.addSubview(labelStackView)
        labelStackView.anchor(top: landscapeImageView.bottomAnchor, leading: roundStackView.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 5, bottom: 0, right: 0), size: CGSize(width: 50, height: 0))
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 8
        labelStackView.axis = .vertical
        
        
        let jsonLabelStackView = UIStackView(arrangedSubviews: [genreLabel, directorLabel, runTimeLabel, releasedLabel])
        topView.addSubview(jsonLabelStackView)
        
        jsonLabelStackView.anchor(top: landscapeImageView.bottomAnchor, leading: nil, bottom: nil, trailing: topView.trailingAnchor, padding: .init(top: 9, left: 0, bottom: 0, right: 0), size: CGSize(width: 150, height: 0))
        
        jsonLabelStackView.distribution = .equalSpacing
        jsonLabelStackView.spacing = 6
        jsonLabelStackView.axis = .vertical
        
        setupViews()
        
    }
    
    fileprivate func setupBottomLabels() {
        
        let roundStackView = UIStackView(arrangedSubviews: [actorRound,storyRound])
        topView.addSubview(roundStackView)
        roundStackView.anchor(top: portraitImageView.bottomAnchor, leading: topView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 12, left: 10, bottom: 0, right: 0), size: CGSize(width: 5, height: 0))
        roundStackView.distribution = .equalSpacing
        roundStackView.spacing = 20 // needs channging into autoresizing according the number of line of sentences
        roundStackView.axis = .vertical
        
        
        let labelStackView = UIStackView(arrangedSubviews: [actor,story])
        topView.addSubview(labelStackView)
        labelStackView.anchor(top: portraitImageView.bottomAnchor, leading: roundStackView.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 7, bottom: 0, right: 0), size: CGSize(width: 50, height: 0))
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 15 // needs channging into autoresizing according the number of line of sentences
        labelStackView.axis = .vertical
        
        
        let jsonLabelStackView = UIStackView(arrangedSubviews: [actorLabel, storyLabel])
        topView.addSubview(jsonLabelStackView)
        
        jsonLabelStackView.anchor(top: portraitImageView.bottomAnchor, leading: labelStackView.trailingAnchor, bottom: nil, trailing: topView.trailingAnchor, padding: .init(top: 9, left: 0, bottom: 0, right: 0), size: CGSize(width: 150, height: 0))
        
        jsonLabelStackView.distribution = .equalSpacing
        jsonLabelStackView.spacing = 1
        jsonLabelStackView.axis = .vertical
    }
    
    
    fileprivate func setViewSettingWithBgShade(view: UIView) {
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: -0.5, height: view.frame.height)
        view.layer.shadowRadius = 2.0
        view.layer.shadowColor = UIColor.white.cgColor
        view.layer.masksToBounds = false
    }
    
    fileprivate func setupImageViews() {
        self.setViewSettingWithBgShade(view: portraitImageView)
        
        topView.addSubview(landscapeImageView)
        landscapeImageView.anchor(top: topView.topAnchor, leading: topView.leadingAnchor, bottom: nil, trailing: topView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 200))
        
        topView.addSubview(portraitImageView)
        portraitImageView.anchor(top: landscapeImageView.topAnchor, leading: landscapeImageView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 120, left: 10, bottom: 0, right: 0), size: CGSize(width: 140, height: 220))
    }
    
    var collectionView: UICollectionView!
    
    fileprivate func setupCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 40, width: view.frame.width, height: 100), collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SingleSnapshotCell.self, forCellWithReuseIdentifier: "cellID")
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = #colorLiteral(red: 0.345725596, green: 0.3542075157, blue: 0.3954209089, alpha: 1)
        self.bottomView.addSubview(collectionView)
        
    }
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CastlabsSDK .with([CastlabsDASH.init(), CastlabsOMA.init()],
                          andLicenseKey: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJidW5kbGVJZCI6Im1lLnVsdWdiZWsuTG9vayIsImtpZCI6MTk2MCwiaWx2IjpmYWxzZX0.Yiw4KzauXNdkWzoDDKMkLkRNXGFsQg9DFgSu9o4v8TRQw-Rh6p_IIhlZfszl75_yX5lf4juljQ4mQDS7yVzFub1hh7uP6ahPrhvetoub8YElUIhSg9hm57yZLZ4a4V0-fMhdSQjfqOLihwNXl-8Cl7VQoZ9E2n_mmZqWdQ74j4o4Ry0heXOrrBLzWjvgi_R4enBT86YkaVSyyupty11dnBeP3UbUTtcKjJSoDPrNKbRZ9QL5XXUAbsFG78tMos7klM0RslxtiIGUyKHaa3PzTiemH22eZF-edlLK0LRBzVEcMSCAInhRKz_vcmulif_wDIFCd293KbR5Ad8zqehNRA",
                          andDelegate: nil)
        
        view.backgroundColor = #colorLiteral(red: 0.345725596, green: 0.3542075157, blue: 0.3954209089, alpha: 1)
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        
        setupImageViews()
        setupBottomLabels()
        setupLabelsAndButtons()
        setupCollectionView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if type == "SINGLE" {
            singleProduct_fetch(productId: productId!)
        }
        else {
            unionInfo_fetch(productId: productId!)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.addSubview(scrollView)
        scrollView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        let height = view.bounds.height
        scrollView.contentSize = CGSize(width: view.bounds.width, height: height + 240)
        scrollView.delegate = self
        scrollView.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        scrollView.addSubview(topView)
        topView.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: nil, trailing: scrollView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: CGSize(width: view.frame.width, height: 500))
        
        
        scrollView.addSubview(bottomView)
        bottomView.anchor(top: topView.bottomAnchor, leading: scrollView.leadingAnchor, bottom: nil, trailing: scrollView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 500))
        
        topView.roundCorners([.bottomLeft, .bottomRight], radius: 30)
        
        bottomView.addSubview(snapshotImageView)
        snapshotImageView.anchor(top: collectionView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 5, left: 15, bottom: 0, right: 15), size: CGSize(width: 0, height: 250))
        
        view.addSubview(imageLabel)
        imageLabel.anchor(top: bottomView.topAnchor, leading: bottomView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 15, left: 15, bottom: 15, right: 0), size: CGSize(width: 100, height: 0))
        
    }
    
    //MARK:- gesture recognizer method
    @objc fileprivate func handleDismiss(gesture: UIPanGestureRecognizer) {
        
        if gesture.state == .changed {
            let translation = gesture.translation(in: view)
            
            print(translation)
            if translation.x > 0 {
                view.transform = CGAffineTransform(translationX: translation.x, y: 0)
                view.alpha = translation.x
            }
        }
            
        else if gesture.state == .ended {
            
            let translation = gesture.translation(in: view)
            
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
                
                if translation.x > 180 {
                    UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                        gesture.view?.removeFromSuperview()
                    }, completion: { (_) in
                    })
                }
                self.view.transform = .identity
            })
        }
    }
    
    //MARK:- API fetch
    var unionInfo: Union_Info? {
        didSet {
            DispatchQueue.main.async {
                if let runtime = self.unionInfo?.duration {
                    let formatter = DateComponentsFormatter()
                    formatter.allowedUnits = [.hour, .minute]
                    formatter.unitsStyle = .short
                    let formattedString = formatter.string(from: TimeInterval(runtime))!
                    self.runTimeLabel.text = formattedString
                }
                if let releaseyear = self.unionInfo?.releaseYear {
                    self.releasedLabel.text = "\(releaseyear)"
                }
                
                self.genreLabel.text = self.unionInfo?.genre
                self.directorLabel.text = self.unionInfo?.director
                self.actorLabel.text = self.unionInfo?.actor
                self.storyLabel.text = self.unionInfo?.description
                
                self.videoController.nameLabel.text = self.unionInfo?.name
                self.name = self.unionInfo?.name
                
                self.snapShotHandler = {cell, indexPath in
                    var string = self.unionInfo!.snapShotUrl
                    string.insert(contentsOf: "_\(indexPath.row + 1)", at: string.index(string.endIndex, offsetBy: -4))
                    
                    let urlString = URL(string: string)
                    cell.imageView.sd_setImage(with: urlString, completed: nil)
                    
                    if indexPath.row == 0 {
                        self.snapshotImageView.sd_setImage(with: urlString, completed: nil)
                    }
                }
                
                self.singleSelectImageHandler = {cell, indexPath in
                    
                    var string = self.unionInfo!.snapShotUrl
                    string.insert(contentsOf: "_\(indexPath.row + 1)", at: string.index(string.endIndex, offsetBy: -4))
                    
                    self.snapshotImageView.sd_setImage(with: URL(string: string), completed: nil)
                }
            }
        }
    }
    
    func unionInfo_fetch(productId: String) {
        
        let unionInfoUrl = "https://api.looktv.mn/api/product/union_info?productId=\(productId)"
        guard let url = URL(string: unionInfoUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to fetch dat: ", err)
                return
            }
            guard let data = data else {return}
            do {
                let rootJSON = try JSONDecoder().decode(Union_Info.self, from: data)
                self.unionInfo = rootJSON
                
                self.videoController.unionInfoList = rootJSON.list
                
                self.unionInfoList = rootJSON.list
                self.get_url(unionInfoList: self.unionInfoList[0])

                count = self.unionInfo!.snapshotCount
                print(rootJSON)

                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
                
            catch let jsonErr {
                print(jsonErr)
            }
            }.resume()
    }
    
    
    var singleProductInfo: Union_Info? {
        didSet {
            DispatchQueue.main.async {
                if let runtime = self.singleProductInfo?.duration {
                    let formatter = DateComponentsFormatter()
                    formatter.allowedUnits = [.hour, .minute]
                    formatter.unitsStyle = .short
                    let formattedString = formatter.string(from: TimeInterval(runtime))!
                    self.runTimeLabel.text = formattedString
                }
                
                if let releaseyear = self.singleProductInfo?.releaseYear {
                    self.releasedLabel.text = "\(releaseyear)"
                }
                
                self.snapShotHandler = {cell, indexPath in
                    
                    var string = self.unionInfo!.snapShotUrl
                    string.insert(contentsOf: "_\(indexPath.row + 1)", at: string.index(string.endIndex, offsetBy: -4))
                    
                    print(string)
                    
                    let urlString = URL(string: string)
                    cell.imageView.sd_setImage(with: urlString, completed: nil)
                    
                    self.snapshotImageView.sd_setImage(with: urlString, completed: nil)
                    
                }
                
                self.singleSelectImageHandler = {cell, indexPath in
                    
                    var string = self.singleProductInfo!.snapShotUrl
                    string.insert(contentsOf: "_\(indexPath.row + 1)", at: string.index(string.endIndex, offsetBy: -4))
                    
                    self.snapshotImageView.sd_setImage(with: URL(string: string), completed: nil)
                }
                
                self.videoController.nameLabel.text = self.unionInfo?.name
                
                self.genreLabel.text = self.singleProductInfo?.genre
                self.directorLabel.text = self.singleProductInfo?.director
                self.actorLabel.text = self.singleProductInfo?.actor
                self.storyLabel.text = self.singleProductInfo?.description
                
                count = self.singleProductInfo!.snapshotCount
                
                self.videoController.nameLabel.text = self.singleProductInfo?.name
                self.name = self.singleProductInfo?.name
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    func singleProduct_fetch(productId: String) {
        
        let unionInfoUrl = "https://api.looktv.mn/api/product/detail?productId=\(productId)"
        guard let url = URL(string: unionInfoUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to fetch dat: ", err)
                return
            }
            guard let data = data else {return}
            do {
                let rootJSON = try JSONDecoder().decode(Union_Info.self, from: data)
                
                self.videoController.unionInfoList = rootJSON.list
                self.singleProductInfo = rootJSON
                
                self.unionInfoList = rootJSON.list
                self.get_url(unionInfoList: self.unionInfoList[0])
            }
                
            catch let jsonErr {
                print(jsonErr)
            }
            }.resume()
    }
    
    //MARK:- handle play download button
    
    var userStatus: Bool = true
    
    @objc func handleDownload() {
        
        if userStatus {
            let loginController = LoginController()
            present(loginController, animated: true)
        }
        else {
            showAlertView()
        }
    }
    var unionInfoList = [UnionInfo_List]()
    var streamUrl: NSString?
    var assetId: String?
    var name: String?
    
    @objc func handlePlay() {
        let vc = CustomDRMPlayerController()
        
        vc.streamUrl = self.streamUrl!
        vc.nameLabel.text = name
        
        let configutation = CLDrmConfiguration()
        configutation.assetId = assetId
        configutation.merchantId = "giitd"
        configutation.sessionId = "a1d1f1p1"
        configutation.userId = "user-test"
        configutation.environment = DrmEnvironment.production
        configutation.type = DrmType.oma
        configutation.tempDirectory = applicationDocumentsDirectory as String
        configutation.workingDirectory = NSTemporaryDirectory()
        vc.drmConfiguration = configutation
        self.present(vc, animated: true, completion: nil)
    }
    
    var video: Video! {
        didSet {
            self.streamUrl = self.video.streamList[0].url as NSString
            self.assetId = self.video.assetId
        }
    }

    func get_url(unionInfoList: UnionInfo_List) {

        let unionInfoUrl = "https://api.looktv.mn/api/product/get_url_new?productId=\(unionInfoList.productId)"
        print(unionInfoUrl, "mpd file")
        guard let url = URL(string: unionInfoUrl) else { return }

        URLSession.shared.dataTask(with: url) { (data, response, err) in

            if let err = err {
                print("Failed to fetch dat: ", err)
                return
            }

            guard let data = data else {return}

            do {
                let rootJSON = try JSONDecoder().decode(Video.self, from: data)
                print(rootJSON)
                self.video = rootJSON
            }

            catch let jsonErr {
                print(jsonErr)
            }
            }.resume()
    }
}
