//
//  CustomDRMPlayerController.swift
//  Look
//
//  Created by Mirzoulugbek Yusupov on 8/27/19.
//  Copyright © 2019 Mirzoulugbek Yusupov. All rights reserved.
//

import MediaPlayer
import UIKit

let AUDIO_TRACKS: NSString = "A_AUDIO_TRACKS"
let SUBS_TRACKS: NSString = "B_SUBS_TRACKs"
let VIDEO_QUALITIES: NSString = "C_VIDEO_QUALITIES"

let LOG_STATES: Bool = false
let TRACK_MENU_ENTRY_DISABLE_KEY: Int = -100
let TRACK_MENU_ENTRY_FORCED_SUBTITLES_KEY: Int = -101

class CustomDRMPlayerController: UIViewController, CLPlayerListenerProtocol {
    
    var type: String?
    
    var streamUrl: NSString = {
        var string = NSString()
        return string
    }()
    
    lazy var playerLayer: CALayer = {
        return self.player!.playerView
    }()
    
    var drmConfiguration: CLDrmConfiguration?
    var player: CLPlayer?
    
    var seekableRangeStart: CMTime = {
        let time = CMTime()
        return time
    }()
    
    var seekableRangeDuration: CMTime = {
        let time = CMTime()
        return time
    }()
    
    let seekedInLiveStream: CMTime = {
        let time = CMTime()
        return time
    }()
    
    var isLive: Bool = false
   
    var labelTimer: Timer?
    
    let errorMessage: NSString = {
        let m = NSString()
        return m
    }()
    
    //MARK: UI elements
 
    let playButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFill
//        button.backgroundColor = .blue
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icon_category_open"), for: .normal)
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFill
        //        button.backgroundColor = .blue
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let seperatorLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.contentMode = .scaleAspectFit
        label.textColor = .white
        label.text = "/"
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.text = "00:00:00"
        label.contentMode = .scaleAspectFit
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let durationLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.text = "00:00:00"
        label.contentMode = .scaleAspectFit
        label.textColor = .white
        label.font = UIFont(name: "Verdana", size: 10)
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.clipsToBounds = true
        label.contentMode = .scaleAspectFit
        label.textColor = .white
        label.text = "Name Label"
        label.font = UIFont(name: "Verdana", size: 16)
        return label
    }()
    
    let activityLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.text = "ACTIVITY"
        label.textColor = .white
        label.textAlignment = .center
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let ac = UIActivityIndicatorView(style: .white)
        ac.hidesWhenStopped = true
        ac.alpha = 0.7
        ac.backgroundColor = .clear
        return ac
    }()
    
    
    lazy var playerSlider: UISlider = {
        let slider = UISlider(frame: CGRect(x: 0, y: 0, width: self.transParentContainerView.frame.width, height: 40))
        slider.tintColor = UIColor(red: 0 / 255, green: 209 / 255, blue:  102 / 255, alpha: 1)
        slider.isContinuous = true
        slider.addTarget(self, action: #selector(sliderDidChange), for: .valueChanged)
//        slider.setThumbImage(UIImage(), for: .normal)
        return slider
    }()
    
    var currentTime: Double {
        get {
            return CMTimeGetSeconds(player!.position)
        }
        set {
            let newTime = CMTimeMakeWithSeconds(newValue, preferredTimescale: 1)
//            player!.seek(to: newTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            player?.seek(newTime)
        }
    }
    
    let transParentContainerView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }()
    
    //MARK: delegate
    func onError(_ errorID: ErrorID, withMessage message: String!) {
        
    }
    
    func onStateChanged(to newState: StateID, from oldState: StateID, withData data: [AnyHashable : Any]!) {
        
        DispatchQueue.main.async {
            switch (newState) {
            case .STATE_IDLE:
                break
            case .STATE_READY:
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.onPlayerReady()
                break
            case .STATE_PLAYING:
                break
            case .STATE_PAUSED:
                break
            case .STATE_SEEKING:
                self.activityIndicator.startAnimating()
                break
            case .STATE_STALLED:
                break
            case .STATE_FINISHED:
                break
            case .STATE_UNKNOWN:
                break
            case .STATE_LOADING:
                self.activityIndicator.startAnimating()
                break
            @unknown default:
                break
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.player == nil {
            loadPlayer()
            player?.open()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange), name: UIApplication.didChangeStatusBarFrameNotification, object: nil)
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    fileprivate func loadPlayer() {
        
        let factory = CLPlayerFactory.init()
        
        if streamUrl.isEqual(to: "") {
            NSLog("ERROR - NO Url specified")
            return
        }
        else {
            player = factory.createPlayer(withStreamUrl: streamUrl as String, andDrmConfiguration: drmConfiguration, andContentType: .contentDASH)
        }
        
        self.player?.addListener(self)
    }
    
  
    fileprivate func onPlayerReady() {
        
        self.player?.play()
        self.centerPlayerView()
        playerLayer.backgroundColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(playerLayer, at: 0)
        
        guard let duration = self.player?.duration else {return}
        let currentTime = self.player?.position
        
        if duration.timescale != 0 {
            self.durationLabel.text = CLPlayerUtils.cmTime(toString: duration)
            self.playerSlider.maximumValue = Float(CMTimeGetSeconds(duration))
            
        }
        else {
            self.playerSlider.isEnabled = false
            self.playerSlider.maximumValue = 0
            self.playerSlider.value = 0
        }

//        self.setupTimers()
    }
   
    
    func createTimeString(time: Float) -> String {
        let components = NSDateComponents()
        components.second = Int(max(0.0, time))
        return timeRemainingFormatter.string(from: components as DateComponents)!
    }
    
    let timeRemainingFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        formatter.allowedUnits = [.hour, .minute, .second]
        return formatter
    }()
    
    fileprivate func centerPlayerView() {
        let fullScreenRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        playerLayer.frame = fullScreenRect
        playerLayer.position = CGPoint(x: fullScreenRect.midX, y: fullScreenRect.midY)
    }
    
    @objc fileprivate func deviceOrientationDidChange(notification: NSNotification) {
        self.centerPlayerView()
    }
  
    fileprivate func setupUI() {
        
        setupTransparentView()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnView)))
        
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(delayedAction), userInfo: nil, repeats: false)
        
        view.addSubview(activityIndicator)
        activityIndicator.centerInSuperview(size: CGSize(width: 40, height: 40))
    }
    
    @objc fileprivate func delayedAction() {
        self.transParentContainerView.removeFromSuperview()
    }
    
    @objc fileprivate func handlePlayButton() {
        let state: StateID = self.player!.state
        if state != .STATE_PLAYING //&& state != .STATE_STALLED
        {
            playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            self.player?.play()
        }else {
            playButton.setImage(#imageLiteral(resourceName: "player_icon"), for: .normal)
            self.player?.pause()
        }
    }
    
    @objc fileprivate func handleDismissButton() {
        self.player?.stop()
        self.player?.removeListener(self)
        self.playerLayer.removeFromSuperlayer()
        
        if type == "PACAKGE" {
            self.dismiss(animated: false, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @objc fileprivate func handleTapOnView() {
        view.addSubview(transParentContainerView)
        transParentContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    @objc fileprivate func handleTapOnTransparent() {
        self.transParentContainerView.removeFromSuperview()
    }
    
    fileprivate func setupTransparentView() {
        
        view.addSubview(transParentContainerView)
        self.transParentContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        transParentContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnTransparent)))
        
        //playbutton
        transParentContainerView.addSubview(playButton)
        playButton.centerInSuperview(size: CGSize(width: 100, height: 100))
        playButton.addTarget(self, action: #selector(handlePlayButton), for: .touchUpInside)
        
        //dismissbutton
        transParentContainerView.addSubview(dismissButton)
        dismissButton.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 5, bottom: 0, right: 0), size: CGSize(width: 50, height: 50))
        dismissButton.addTarget(self, action: #selector(handleDismissButton), for: .touchUpInside)
        
        
        //slider and label
        transParentContainerView.addSubview(nameLabel)
        nameLabel.centerXInSuperview()
        nameLabel.anchor(top: transParentContainerView.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 0))
        
        transParentContainerView.addSubview(playerSlider)
        playerSlider.anchor(top: nil, leading: transParentContainerView.leadingAnchor, bottom: transParentContainerView.bottomAnchor, trailing: transParentContainerView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 40, right: 0), size: CGSize(width: 0, height: 0))
        
        transParentContainerView.addSubview(currentTimeLabel)
        currentTimeLabel.anchor(top: playerSlider.bottomAnchor, leading: transParentContainerView.leadingAnchor, bottom: transParentContainerView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 0, right: 0), size: CGSize(width: 50, height: 0))
        
        transParentContainerView.addSubview(seperatorLabel)
        seperatorLabel.anchor(top: playerSlider.bottomAnchor, leading: currentTimeLabel.trailingAnchor, bottom: transParentContainerView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 0))
        
        transParentContainerView.addSubview(durationLabel)
        durationLabel.anchor(top: playerSlider.bottomAnchor, leading: seperatorLabel.trailingAnchor, bottom: transParentContainerView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 6, bottom: 0, right: 0), size: CGSize(width: 50, height: 0))
    }
    
    @objc func sliderDidChange(_ sender: UISlider) {
        let seekTime = CMTimeMakeWithSeconds(Float64(playerSlider.value), preferredTimescale: 1000)
        playerSlider.value = Float(CMTimeGetSeconds(seekTime))
        currentTimeLabel.text = CLPlayerUtils.cmTime(toString: seekTime)
        self.player?.seek(seekTime)
    }
}
