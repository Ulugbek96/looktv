//
//  login.swift
//  Look
//
//  Created by Mirzoulugbek Yusupov on 8/14/19.
//  Copyright © 2019 Mirzoulugbek Yusupov. All rights reserved.
//
import UIKit

struct SignupMessage: Decodable {
    let resultCode: String
    let message: String
}
